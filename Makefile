
.PHONY: all view biblio

rapport.pdf: rapport.tex
	pdflatex rapport.tex

presentation.pdf: presentation.tex
	pdflatex presentation.tex

biblio: presentation.bib presentation.pdf rapport.pdf
	bibtex   presentation.aux
	bibtex   rapport.aux
	pdflatex presentation.tex
	pdflatex rapport.tex

view: presentation.pdf
	open presentation.pdf

all: view
	wait 0
