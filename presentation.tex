\documentclass[a4paper,10pt]{beamer}

\usecolortheme{whale}
\usecolortheme{orchid}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath,amssymb}%for \mod, \bmod, ..
\usepackage{amsthm,amsfonts,mathrsfs}
\usepackage[full,small]{complexity}
\usepackage{ stmaryrd }
\usepackage{ marvosym }

%pour le tableau récapitualif 
\usepackage{hyperref}

\usepackage{enumerate,multicol}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}

\newcommand{\Nat}{{\mathbb{N}}}
\newcommand{\eqv}{\Leftrightarrow}
\newcommand{\ds}{\vspace{0.5\baselineskip}}

% Plan des sections et numéros des sections
\AtBeginSubsection[]{
  \frame{ 
    \frametitle{Plan}   
    \tableofcontents[currentsection,currentsubsection]  }}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]

% Titre
\title{\textbf{Transducteurs \& Logique} \\ Un rapide survey}
\author{Lopez Aliaume \and Douéneau Gaëtan}
\date{30 novembre 2016}

\begin{document}

\maketitle 


\begin{frame}
\frametitle{Décision vs Calcul}
    \begin{center}
        \begin{tikzpicture}
        
        	
            \draw[->, thick,red] (8,-3.5) -- (1,-3.5) node[midway,above] {Expressivité};
            \draw (0,0) node[right] {Machine de Turing};

            \draw[blue] (9,2.5) node {Modèle de décision};
            \draw[dashed,blue] (3,0) -- (10,0);

	    \draw (4,0.5)  node {Automate boustrophédon};
            \draw (6,1)  node {Automate non-déterministe};
            \draw (8,1.5)  node {Automate déterministe};

        \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Décision vs Calcul}
    
    \begin{block}{Propriété: Problème de décision}
        Un problème de décision d'une propriété $\phi$
        revient à décider l'appartenance à un langage $L = \{ w ~|~ w \vDash \phi \}$.
    \end{block}

    Idée: limiter la complexité du modèle.

    \begin{exampleblock}{Avec une entrée en lecture seule}
        Les modèles de décision sur une \textbf{entrée en lecture seule}
        et avec \textbf{une seule bande}
        se ramènent à un automate fini déterministe. Par exemple~:

        \begin{enumerate}[(i)]
            \item automate non déterministe (NFA) 
            \item automate à deux sens (Boustrophédon) \cite{jc1959}
            \item machine de Turing qui s'écrit pas sur son entrée \cite{hopcroft1969}
        \end{enumerate}
    \end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Décision vs Calcul}
    \begin{center}
        \begin{tikzpicture}
        
        	
            \draw[->, thick,red] (8,-3.5) -- (1,-3.5) node[midway,above] {Expressivité};
            \draw (0,0) node[right] {Machine de Turing};

            \draw[blue] (9,2.5) node {Modèle de décision};
            \draw[dashed,blue] (3,0) -- (10,0);
            
            \draw[thick,red]  (2,0.3) rectangle (9.8,1.8);

	    \draw (4,0.5)  node {Automate boustrophédon};
            \draw (6,1)  node {Automate non-déterministe};
            \draw (8,1.5)  node {Automate déterministe};
            
            \onslide<2->
            \draw (4,-0.5) node {Transducteur à deux sens};
            \draw (6,-1) node {Transducteur rationnel};
            \draw (8,-1.5) node {Transducteur séquentiel};
            
            \draw[blue] (9,-2.5) node {Modèle de calcul};

        \end{tikzpicture}
    \end{center}
\end{frame}


\begin{frame}
    \frametitle{Décision vs Calcul}

    \begin{exampleblock}{Calcul non déterministe}
        Introduire du non déterminisme dans un problème de calcul 
        n'est pas anodin~: fonction $\neq$ relation.
    \end{exampleblock}

    \begin{exampleblock}{Calcul à deux sens}
        Autoriser le déplacement dans les deux sens 
        sur l'entrée n'est pas anodin~: 
        les transducteurs déterministes à deux sens contiennent 
        \textbf{strictement} les transducteurs déterministes \cite{filiot2013}.
        \vspace{1em}
    \end{exampleblock}
\end{frame}


\begin{frame}
\frametitle{Décision vs Calcul}
    \begin{center}
        \begin{tikzpicture}
        
        	
            \draw[->, thick,red] (8,-3.5) -- (1,-3.5) node[midway,above] {Expressivité};
            \draw (0,0) node[right] {Machine de Turing};

            \draw[blue] (9,2.5) node {Modèle de décision};
            \draw[dashed,blue] (3,0) -- (10,0);
            
            \draw[thick,red]  (2,0.3) rectangle (9.8,1.8);
	    \draw (4,0.5)  node {Automate boustrophédon};
            \draw (6,1)  node {Automate non-déterministe};
            \draw (8,1.5)  node {Automate déterministe};

	    \draw[thick,red]  (2,-0.3) rectangle (6.1,-0.7);
            \draw (4,-0.5) node {Transducteur à deux sens};
	    \draw[thick,red]  (4.2,-0.8) rectangle (7.9,-1.2);
            \draw (6,-1) node {Transducteur rationnel};
	    \draw[thick,red]  (6.3,-1.3) rectangle (9.7,-1.7);
            \draw (8,-1.5) node {Transducteur sequentiel};
            
            \draw[blue] (9,-2.5) node {Modèle de calcul};

        \end{tikzpicture}
    \end{center}
\end{frame}
\begin{frame}
    \frametitle{Caractérisations logiques}

    \begin{block}{Caractérisations logiques}
        \begin{enumerate}[(i)]
            \item Automates $\leftrightarrow$ MSO (sur les mots)
            \item Automates sans compteurs $\leftrightarrow$ FO
        \end{enumerate}
    \end{block}
  

    \begin{exampleblock}{Question}
        Peut-on avoir une classification logique des fonctions 
        codées par des transducteurs ? Est-elle semblable ?
    \end{exampleblock}
    
       \onslide<2->
    
        \begin{exampleblock}{Réponse}
        Oui, mais c'est plus compliqué.
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Motivations}


    \begin{alertblock}{Utilité pratique}
        La caractérisation logique fournit un cadre particulièrement adapté pour faire de l'\textbf{analyse de programmes.}
    \end{alertblock}
    
        \begin{exampleblock}{Exemple}
         Décider l'équivalence de programmes d'un certain type.
         \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Un domaine récent}

    
        \begin{block}{Logique et automates finis}
         Résultats assez anciens : années 1950-60.
         Une théorie "complète".
         \end{block}

         
        \begin{alertblock}{Logique et transducteurs : papier fondateur}
        \cite{engelfriet2001} : transductions MSO.
        \end{alertblock}
        
        \begin{block}{Logique et transducteurs}
         Résultats depuis 2000, et surtout 2010-aujourd'hui.
         
         $\rightarrow$ Beaucoup de questions ouvertes.
         \end{block}
\end{frame}





\begin{frame}
    \frametitle{Laboratoires}
    
        \begin{exampleblock}{Equipes actuellement sur le sujet}
        \begin{itemize}
        \item Automates et applications - IRIF (Université Paris 7)
        \item Méthodes formelles - LaBRI (Université de Bordeaux)
         \item Regular Functions and Quantitative Properties of Data Streams - Université de Pennsylvanie
         \item Le Laboratoire d’Informatique Fondamentale de Marseille - LIF Marseille
         \item L'Université libre de Bruxelles 
        
        \end{itemize}
        \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Publications}
    
        \begin{exampleblock}{Conférences}
        \begin{itemize}
        \item Computer Science Logic (CSL)
        \item Principles Of Programming Languages (POPL)
         \item Logic In Computer Science (LICS)
         \item International Colloquium on Automata, Languages, And Programming (ICALP)
         \item Developments in Language Theory (DLT)
        
        \end{itemize}
        \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Un auteur célèbre}
    
    \begin{multicols}{2}
    
   \includegraphics[width=5cm]{alur.jpg}
   
   \columnbreak
   
   \vspace*{2.2cm}
Rajeev Alur

Université de Pennsylvanie

\ds

(automates temporisés)

\end{multicols}

\end{frame}







\begin{frame}
\frametitle{Plan}
\tableofcontents
\end{frame}

\section{Paysage général de la théorie des transducteurs}

\subsection{Transductions à sens unique}

\begin{frame}
    \frametitle{Transducteurs synchrones}

    \begin{block}{Définition: Transducteur Synchrone}
        Un transducteur synchrone est un automate 
        non déterministe qui écrit une lettre à chaque 
        lettre lue.
        
        Il a un alphabet d'entrée $A$ et un de sortie $B$.
    \end{block}

    \begin{alertblock}{Caractérisation}
        Le relations décrites sur $A^*\times B^*$ par les transducteurs synchrones sont exactement les langages rationnels de $(A \times B)^*$.
        
        \[
            \textrm{sync}(A,B)  = \textrm{rat}(A \times B)
        \]
    \end{alertblock}

\end{frame}

\begin{frame}
    \frametitle{Transducteurs synchrones}

    \begin{exampleblock}{Exemple}
       Étant donné une fonction de renommage
       \[
           \rho : A \rightarrow B
       \] 
       
       On peut écrire un transducteur synchrone qui reconnaît 
       
       \[ 
           R = \{(w, \rho(w))à~|~w \in A^*\}
       \]
       
      \end{exampleblock}

\end{frame}

\begin{frame}
    \begin{alertblock}{Attention}
        Du fait du non-déterminisme, un transducteur synchrone 
        calcule une \emph{relation}, et pas a priori une \emph{fonction}.
    \end{alertblock}
    
    \begin{block}{Notation}
        Pour un ensemble de relations $F$ on notera $\textrm{functions}(F)$ 
        l'ensemble des relations de $F$ qui sont aussi des fonctions.
    \end{block}
\end{frame}



\begin{frame}
    \frametitle{Fonctions séquentielles (DFT)}

    \begin{block}{Défintion: Deterministic Finite Transducer}
        Une fonction séquentielle de $A$ vers $B$
        est définie par un automate déterministe sur $A$ 
        accompagné de trois fonctions $m, \gamma, \rho$. 

        \begin{enumerate}[(i)]
            \item Une fonction de préfixe

        \[
            m : Q \rightarrow B^* 
        \]

            \item Une fonction de production
        \[
            \gamma : Q \times A \rightarrow B^* 
        \]
            \item Une fonction de suffixe

        \[
            \rho : Q \rightarrow B^*
        \]
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Fonctions séquentielles (DFT)}

    
    \begin{exampleblock}{Exemple : calcul de l'addition en binaire}
        \begin{center}
            \begin{tikzpicture}

                \node[state, initial] (S0) {$0$};
                \node[state] (S1) [right = 4cm of S0] {$1$};
                \node (E0) [above = 1cm of S0] {$\varepsilon$};
                \node (E1) [above = 1cm of S1] {$1$};

                \draw[->] (S0) edge[bend left=45] node[midway, above] {$(1,1)~|~0$}  (S1)
                          (S1) edge[bend left=45] node[midway,below] {$(0,0)~|~1$} (S0);

                  \draw[->] (S0) edge[loop below] node[midway, below] {$(x,y) ~|~ x+y$} (S0);
                  \draw[->] (S1) edge[loop below] node[midway, below] {$(x,y) ~|~ \neg (x+y)$} (S1);

                  \draw[->] (S0) edge (E0);
                  \draw[->] (S1) edge (E1);

            \end{tikzpicture}
        \end{center}
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Fonctions séquentielles (DFT)}

    \begin{alertblock}{Expressivité}
        Les fonctions synchrones sont strictement moins expressives que les fonctions séquentielles.
        
        \[
            \textrm{functions}(\textrm{sync}(A,B)) \subsetneq \textrm{DFT}(A,B) 
        \]
    \end{alertblock}
    
    \begin{exampleblock}{Interprétation}
        Les fonctions séquentielles sont bien plus expressives que de simples langages rationnels.
    \end{exampleblock}

\end{frame}




\begin{frame}
    \frametitle{Transductions rationnelles (NFT)}

    \begin{block}{Défintion: Non-deterministic Finite Transducer}
        Une relation rationnelle de $A$ vers $B$
        est définie par un automate \textbf{non déterministe} sur $A$ 
        accompagné de trois fonctions $m, \gamma, \rho$. 

        \begin{enumerate}[(i)]
            \item Une fonction de préfixe

        \[
            m : Q \rightarrow B^* 
        \]

            \item Une fonction de production
        \[
            \gamma : Q \times A \rightarrow B^* 
        \]
            \item Une fonction de suffixe

        \[
            \rho : Q \rightarrow B^*
        \]
        \end{enumerate}
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Transductions rationnelles (NFT)}

    \begin{alertblock}{Inclusion stricte}
        \[
            \textrm{DFT}(A,B) \subsetneq \textrm{functions}(\textrm{NFT}(A,B))
        \]  
    \end{alertblock}

    \begin{exampleblock}{Témoin de l'inclusion stricte}
        \[
            f : u \mapsto \begin{cases}
                a^{|u|} & \textrm{ quand } u \textrm{ termine par } a \\
                b^{|u|} & \textrm{ quand } u \textrm{ termine par } b \\
            \end{cases}
        \]


        La fonction $f$ est bien définie, mais n'est pas séquentielle (résiduels).
        Mais $f$ peut se représenter avec un NFT.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Transductions rationnelles (NFT)}

        
        \begin{exampleblock}{Témoin de l'inclusion stricte}
            \begin{center}
                \begin{tikzpicture}
                    \node[state,initial] (S0)  {$\bot$};
                    \node[state] (SA)  [above right = 1cm of S0] {$a$};
                    \node[state] (SAF) [right = 1cm of SA]  {$a_f$};
                    \node (AF)  [right = 1cm of SAF] {$\varepsilon$};

                    \node[state] (SB)  [below right = 1cm of S0] {$b$};
                    \node[state] (SBF) [right = 1cm of SB]  {$b_f$};
                    \node (BF)  [right = 1cm of SBF] {$\varepsilon$};

                    \draw[->]
                        (S0) edge [bend left=45]  node[midway,above = 0.3cm] {$\alpha~|~a$} (SA)
                        (S0) edge [bend right=45] node[midway,below = 0.3cm] {$\alpha~|~b$} (SB)
                        (SA) edge [loop above] node[midway,above] {$\alpha~|~a$} (SA)
                        (SB) edge [loop below] node[midway,below] {$\alpha~|~a$} (SB)
                        (SA) edge node[midway,above] {$a~|~a$} (SAF)
                        (SB) edge node[midway,below] {$b~|~b$} (SBF)
                        (SAF) edge  (AF)
                        (SBF) edge  (BF);
                \end{tikzpicture}
            \end{center}
        \end{exampleblock}

\end{frame}


\begin{frame}
    \frametitle{Pour résumer}

    \begin{alertblock}{Expressivité}


        \[
            \textrm{function}(\textrm{sync}(A,B))  \subsetneq \textrm{DFT}(A,B)  \subsetneq \textrm{functions}(\textrm{NFT}(A,B) )
        \]
        
    \end{alertblock}
    
\end{frame}


\subsection{Transducteurs déterministes à deux sens}

\begin{frame}
    \frametitle{Un sens \emph{vs} Deux sens}
    
    \begin{exampleblock}{Intuition: Transducteur déterministe à un sens (DFT)}

        \begin{itemize}
            \item un ensemble fini d'états
            \item une bande d'entrée en \emph{lecture seule} (qui est parcourue dans \textbf{un seul sens})
            \item une bande de sortie en \emph{écriture seule}
        \end{itemize}

    \end{exampleblock}

    \begin{exampleblock}{Intuition: Transducteur déterministe à deux sens (2DFT)}

        \begin{itemize}
            \item un ensemble fini d'états
            \item une bande d'entrée en \emph{lecture seule} (qui peut être parcourue dans les \textbf{deux sens})
            \item une bande de sortie en \emph{écriture seule}
        \end{itemize}

    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Deux sens \emph{vs} Machine de Turing}


    \begin{exampleblock}{Intuition: Transducteur déterministe à deux sens (2DFT) } 

        \begin{itemize}
            \item un ensemble fini d'états
            \item une bande d'entrée en \emph{lecture seule} (qui peut être parcourue dans les deux sens)
            \item une bande de sortie en \textbf{{écriture seule}}
        \end{itemize}

    \end{exampleblock}
    
        \begin{exampleblock}{Intuition: Machine de Turing}

        \begin{itemize}
            \item un ensemble fini d'états
            \item une bande d'entrée en \emph{lecture seule} (qui peut être parcourue dans les deux sens)
            \item une bande de \textbf{sortie/calcul (lue et écrite)}
        \end{itemize}

    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Machine de Turing}
    
    \begin{block}{Définition: Machine de Turing}
        \label{def:machine-turing}
        
        Une machine de Turing (déterministe) $\mathcal{M}$ est un 7-uplet 
        $\mathcal{M} = \langle Q, A , B, \delta, \gamma, q_0, F \rangle$ 
        avec~:
        \begin{itemize}
            \item $Q$ est un ensemble fini d'états 
            \item $A$ et $B$ sont respectivement l'alphabet d'entrée 
                et de sortie (travail) de la machine de turing  
            \item $\delta : Q \times (A \uplus \{ \vdash, \square \}) \times (B \uplus \{ \vdash, \square \})  \rightarrow Q \times \{ -1, 0, +1 \}^2$
                est la fonction de transition
            \item $\gamma : Q \times (A \uplus \{ \vdash, \square \}) \times (B \uplus \{ \vdash, \square \}) \rightarrow A \times B$
                est la fonction de production 
            \item $q_0 \in Q$ est l'état initial 
            \item $F \subseteq Q$ est l'ensemble d'états finaux
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Transducteur déterministe à deux sens}

    \begin{block}{Définition: 2DFT  \cite{engelfriet2001}}
        \label{def:2-way-trans}
        Un transducteur déterministe à deux sens $\mathcal{A}$ est un 
        7-uplet $\langle Q, A, B, \delta, \gamma, q_0, F \rangle$ avec~:

        \begin{itemize}
            \item $Q$ est un ensemble fini d'états 
            \item $A$ et $B$ sont respectivement l'alphabet d'entrée 
                et de sortie du transducteur 
            \item $\delta : Q \times (A \uplus \{ \vdash, \dashv \}) \rightarrow Q \times \{ -1, 0, +1 \}$
                est la fonction de transition
            \item $\gamma : Q \times (A \uplus \{ \vdash, \dashv \}) \rightarrow B^*$
                est la fonction de production 
            \item $q_0 \in Q$ est l'état initial 
            \item $F \subseteq Q$ est l'ensemble d'états finaux
        \end{itemize}

    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Exemple de transduction 2DFT}
    

    \begin{exampleblock}{Un exemple de transduction à deux sens}
        \[
            f(a^{k_0} b a^{k_1} b \dots a^{k_n}b) = a^{k_0} b^{k_0} \dots b^{k_{n-1}} a^{k_n} b^{k_n}
        \]

        \begin{center}
            \includegraphics[width=4cm]{Articles/exemple_carton2015.pdf}
        \end{center}
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Vers les transducteurs à deux sens non-déterministes ?}

    \begin{alertblock}{Théorème : 2NFT \cite{engelfriet2001}}
        Ajouter du non déterminisme aux 2DFT n'ajoute pas d'expressivité
        dans le cas des fonctions :
        
        \[
            \text{2DFT} = \textrm{functions}(\text{2NFT})
        \]
       
    \end{alertblock}
    
    \ds
    
       Ainsi nous ne parlerons pas de 2NFT.
       
       
\end{frame}


\begin{frame}
    \frametitle{NFT \emph{vs} 2DFT}

    \begin{alertblock}{Théorème : \cite{filiot2013}}
    
    \[ \textrm{functions}(\text{NFT}) \subsetneq  \text{2DFT} \]


    \end{alertblock}
    
    \ds
    
    Idée de la preuve : on peut "vérifier" les hypothèses non-déterministes, quitte à se déplacer en avant puis en arrière.
    

\end{frame}

\begin{frame}
    \frametitle{Pour résumer}

    \begin{alertblock}{Théorème : Hiérarchie}
    
    \[
        \text{DFT}\subsetneq \textrm{functions}(\text{NFT}) \subsetneq  \text{2DFT} = \textrm{functions}(\text{2NFT}) 
    \]


    \end{alertblock}
    
    \begin{exampleblock}{Bonus \cite{filiot2013}}
        Étant donné un 2DFT,
        on peut décider si la fonction qu'il code est dans DFT, 
        dans \textrm{functions}(NFT)$\smallsetminus$ DFT, ou dans 
        2DFT $\smallsetminus$ \textrm{functions}(NFT).
    \end{exampleblock}
\end{frame}










\section{Logique et transducteurs : résultats récents}




\subsection{MSO et 2DFT}

\begin{frame}[fragile]
    \frametitle{Caractérisation intuitive des 2DFT}

    \begin{exampleblock}{Traduction informelle en code} 
        \label{def:2-way-informel}
        \begin{verbatim}
    let etat0 = 
        let l = lire_lettre
        if check l then
            write a1
            move d1
            etat1
        else
            write a2
            move d2
            etat2
        \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Déplacements sur le mot}

    \begin{alertblock}{Formalisation}
        Un transducteur est un ensemble fini de fonctions 
        mutuellement récursives $(f_{q_i})_i$,
        qui peuvent utiliser les fonctions~:

        \begin{enumerate}[(i)]
            \item $\textrm{check} : A \rightarrow \{ 0, 1 \}$
            \item $\textrm{move} : \{ -1, 0, +1 \} \rightarrow ()$
            \item $\textrm{write} : B^* \rightarrow ()$
        \end{enumerate}
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Déplacements sur le mot}

    \begin{exampleblock}{Transitions généralisées}
        \[ \textrm{check} : A \rightarrow \{ 0, 1 \} \]
        
        Est transformé en~:

        \[
            \textrm{check} : MSO(x) \times P \rightarrow \{ 0, 1 \}
        \]

        Avec~:

        \[
            \textrm{check}(\phi(x),\textrm{position}) = 1 \text{ ssi }
            w,  [x \mapsto \textrm{position}]\models \phi(x)
        \]
    \end{exampleblock}
       

\end{frame}

\begin{frame}[fragile]
    \frametitle{Caractérisation intuitive des 2DFT}

    \begin{exampleblock}{Traduction informelle en code} 
        \label{def:2-way-informel}
        \begin{verbatim}
    let etat0 = 
        let p = lire_position
        if check (phi,p) then
            write a1
            move d1
            etat1
        else
            write a2
            move d2
            etat2
        \end{verbatim}
    \end{exampleblock}
\end{frame}


\begin{frame}

 \frametitle{Caractérisation MSO}
 
         \begin{alertblock}{Equivalence \cite{engelfriet2001}} 
        Les 2DFT sont expressivement équivalents aux transducteurs déterministes à deux sens qui peuvent tester des formules MSO.

    \end{alertblock}


        \begin{exampleblock}{Intuition} 
        On a réussi à introduire du MSO\dots~mais ce n'est pas gagné.
    
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Caractérisation MSO}

    \begin{block}{MSO-transduction} 
        Logique sur les couples de mots : ensemble de formules MSO qui disent comment passer d'un mot à l'autre.
        
        \ds
        
        MSO-transduction : permet de représenter une fonction par des formules.
    
    \end{block}
    


\onslide<2->

    \begin{alertblock}{Caractérisation \cite{engelfriet2001}} 
        Les fonctions définissables par des MSO-transductions sont exactement les fonctions définies par les 2DFT.
        

    \end{alertblock}
\end{frame}







\subsection{Une logique pour functions(NFT)}


\begin{frame}
    \frametitle{functions(NFT) \emph{vs} 2DFT}
   

    \begin{exampleblock}{Exemple}
    
        Soit $R_{copy} = \{(w,ww)~|~w \in A^*\}$
        
        \ds
        
        \begin{itemize}
            \item $R_{copy} \in$ 2DFT
            
            (copier le mot une fois, puis revenir en arrière et le recopier)
            
              \item mais $R_{copy} \not \in$ \textrm{functions}(NFT).
        \end{itemize}
    \end{exampleblock}    
    
     \onslide<2->
     
         \begin{alertblock}{Intuition}
    
        \textrm{functions}(NFT) n'a pas besoin de connaître "trop" de futur pour écrire : elle "préserve l'ordre".
    \end{alertblock}   

\end{frame}

\begin{frame}
    \frametitle{MSO$_{op}$}
   

    \begin{exampleblock}{Intuition}
    
     MSO$_{op}$-transductions : les MSO-transductions qui préservent l'ordre.
     
     \ds
     
     $\rightarrow$ Restriction syntaxique "naturelle" sur les formules.
    \end{exampleblock}    
    
    \onslide<2->
    
        \begin{alertblock}{Caractérisation \cite{engelfriet2001}}
    
     Les fonctions définissables par des MSO$_{op}$-transductions sont exactement les fonctions de \textrm{functions}(NFT).
    \end{alertblock}

\end{frame}



\subsection{Aperiodicité}


\begin{frame}
    \frametitle{FO \emph{vs} MSO}
    \begin{block}{Rappel :  automates finis}
        \begin{enumerate}[(i)]
            \item Automates $\leftrightarrow$ MSO (sur les mots)
            \item Automates sans compteurs $\leftrightarrow$ FO
        \end{enumerate}
    \end{block}
    
        \begin{alertblock}{Transducteurs}
        \begin{enumerate}[(i)]
            \item 2DFT $\leftrightarrow$ MSO-transductions
            \item \textrm{functions}(NFT) $\leftrightarrow$ MSO$_{op}$-transductions
            \item ???? $\leftrightarrow$ FO-transductions
            \item ???? $\leftrightarrow$ FO$_{op}$-transductions
        \end{enumerate}
    \end{alertblock}
\end{frame}


\begin{frame}
    \frametitle{FO-transductions}
    \begin{block}{FO-transduction}
        Ensemble de formules FO pour définir une fonction des mots vers les mots.
    \end{block}

    \begin{block}{FO$_{op}$-transduction}
        Même hypothèse de préservation de l'ordre que pour les MSO$_{op}$-transductions.
    \end{block}
    
\end{frame}

\begin{frame}
    \frametitle{Apériodicité}
    \begin{block}{Transducteur à un sens apériodique \cite{filiot2016}}
        Un NFT est dit \emph{sans compteur} / \emph{apériodique} si l'automate sous-jacent (en oubliant les sorties) l'est.
        
        On note ces transducteurs : NFT$_{AP}$.
    \end{block}
    
    \begin{exampleblock}{Transducteur à deux sens déterministe apériodique}
        C'est plus compliqué à cause de la possibilité de revenir en arrière, mais on a une définition semblable \cite{carton2015}.
        
         On note ces transducteurs : 2DFT$_{AP}$.
    \end{exampleblock}
    
\end{frame}

\begin{frame}
    \frametitle{Apériodicité}
    \begin{alertblock}{Théorème : \cite{filiot2016}}
        Les fonctions de \textrm{functions}(NFT$_{AP}$) sont exactement les fonctions définissables par une FO$_{AP}$-transduction.
    \end{alertblock}
    
    \frametitle{Apériodicité}
    \begin{alertblock}{Théorème : \cite{carton2015}}
        Les fonctions de 2DFT$_{AP}$ sont exactement les fonctions définissables par une FO-transduction.
    \end{alertblock}
    
\end{frame}


\begin{frame}
    \frametitle{Pour résumer}

    \begin{block}{Hiérarchie}

        \begin{center}
            \begin{tikzpicture}
                \node (DFT2)   {2DFT};
                \node (DFTAP2) [below right= 1.5cm of DFT2, midway] {2DFT$_{AP}$};
                \node (FNFT)   [below left = 1.5cm of DFT2, midway] {$\textrm{functions}$(NFT)};
                \node (NFT)    [below = 3cm of DFT2, midway] {$\textrm{functions}($NFT$_{AP})$};

                \draw[->]
                    (NFT) edge (FNFT)
                    (NFT) edge (DFTAP2)
                    (DFTAP2) edge (DFT2)
                    (FNFT) edge (DFT2);

            \end{tikzpicture}
        \end{center}
    \end{block}
\end{frame}




\begin{frame}
    \frametitle{Pour résumer}
    
    \begin{alertblock}{Théorème : Expressivité}
    
        \begin{tabular}{lcl|l}
            2DFT & $\leftrightarrow$ & MSO  & \cite{engelfriet2001} \\
            \textrm{functions}(NFT)          & $\leftrightarrow$ & $\text{MSO}_{op}$ & \cite{filiot2015} \\
            2DFT$_{AP}$  & $\leftrightarrow$ & FO  & \cite{carton2015} \\
            \textrm{functions}(NFT$_{AP}$)   & $\leftrightarrow$ & $\text{FO}_{op}$  & \cite{filiot2016} \\
        \end{tabular}
    \end{alertblock}
    
        \begin{exampleblock}{Bonus : \cite{filiot2016}}
    
        Étant donnée une fonction de \textrm{functions}(NFT), on peut décider si elle est dans \textrm{functions}(NFT$_{AP}$) (réalisable par un transducteur apériodique).
        
    \end{exampleblock}
    
    \ds
    
    Le problème est ouvert pour 2DFT et 2DFT$_{AP}$ \cite{carton2015}.

\end{frame}







\section{Application à la vérification}



\subsection{Streaming string transducers}

\begin{frame}
    \frametitle{Streaming string transducers}

    \begin{block}{Définition: Streaming transducers \cite{alur2010}}
        C'est un automate fini auquel on ajoute~:
        \begin{enumerate}[(i)]
            \item Un ensemble fini de registres $X$ 
            \item Un domaine $D$ de valeurs pour les registres 
            \item Un ensemble fini de fonctions sur $D$
            \item Une fonction de mise à jour $\rho$
                \[
                    \rho : Q \times \Sigma \rightarrow \left[ X \rightarrow \mathcal{T}(X, f_1, \dots, f_n) \right]
                \]
            \item Une fonction d'évaluation $\mathcal{F}$ qui est une fonction 
                de $Q$ vers $\mathcal{T}(X, f_1, \dots, f_n)$
        \end{enumerate}
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Streaming string transducers}
    \begin{exampleblock}{Exemple avec une sortie de taille exponentielle}
         Automate $\mathcal{A}$ avec un seul état $q$ qui boucle, un 
         seul registre $X_1$, 
         la fonction $\rho(q,\alpha,X_1) = X_1 \cdot \alpha \cdot X_1$,
         et la fonction $\mathcal{F}(X_1) = X_1$.

         Sur $abc$ l'automate produit le mot $abacaba$.
    \end{exampleblock}

    \begin{alertblock}{Sortie de taille exponentielle}
        Des restrictions doivent être ajoutées pour limiter l'expressivité du modèle 
        de calcul, des exemples étudiés sont les suivants \cite{alur2013}


        \begin{enumerate}[(i)]
            \item Chaque registre est utilisé au plus une fois dans une règle de production 
            \item Chaque registre est copié au plus un nombre borné de fois
        \end{enumerate}
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Streaming string transducers}

    \begin{exampleblock}{Exemple: insertion dans une liste triée}
        On part d'un mot $w = d_1 \bullet d_2 \bullet \dots \bullet d_n$,
        où $d_2 < \dots < d_n$, on veut insérer $d_1$ pour que $w'$ soit trié.

        \begin{description}
            \item[Registres~:] 
                \begin{itemize}
                    \item $X_1$ qui contient la donnée à insérer 
                    \item $X_2$ qui contient la donnée courante 
                    \item $X_3$ qui contient la chaine construite 
                \end{itemize}
            \item[Domaine~:] $D = A^*$
            \item[Fonctions~:] $f_1$ qui représente la concaténation
            \item[Mise à jour~:] voir slide suivante !
            \item[Évaluation~:] $\mathcal{F}(X_1,X_2,X_3) = X_3 [f_1] X_2 [f_1] X_1$
        \end{description}
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Streaming string transducers}

    \begin{exampleblock}{Exemple:insertion dans une liste triée}
        Fonction de mise à jour~:
        \begin{description}
            \item[Initialement~:] ajoute les lettres dans le registre $X_1$
            \item[Après le premier $\bullet$~:] ajoute les lettres lues dans le registre $X_2$
                sauf quand il croise un $\bullet$, et alors il compare 
                le registre $X_1$ et $X_2$ et effectue le calcul suivant~: 
                \begin{description}
                    \item[Si $X_1 \geq X_2$~:] 
                        \begin{itemize}
                            \item $X_1 := X_1$
                            \item $X_2 := \varepsilon$
                            \item $X_3 := X_3 [f_1] X_2$
                        \end{itemize}
                    \item[Si $X_1 < X_2$~:]
                        \begin{itemize}
                            \item $X_1 := \varepsilon$
                            \item $X_2 := \varepsilon$
                            \item $X_3 := X_3 [f_1] X_1 [f_1] X_2$
                        \end{itemize}
                \end{description}
        \end{description}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Expressivité des SST}

    \begin{alertblock}{Théorème : \cite{alur2010}}
       \[ \text{SST} = \text{2DFT} \]
       
       et le passage d'un modèle à l'autre est effectif.
    \end{alertblock}

    \begin{exampleblock}{Corollaire}
        L'expressivité des \textit{streaming string 
        transducers} est équivalente à celle des MSO-transductions.
    \end{exampleblock}
\end{frame}

\subsection{Analyse de programmes}

\begin{frame}
    \frametitle{Streaming string transducers: Compilation}

    \begin{alertblock}{Programmes $\rightarrow$ SST \cite{alur2011}}
        Dans un langage fonctionnel, soit une fonction $ f : \textrm{Liste} \rightarrow \textrm{Liste}$


        qui traverse une seule fois la liste en entrée.
\ds

Alors il existe un \textrm{SST}  $S$ tel que
        \[
            \llbracket S \rrbracket = \llbracket f \rrbracket  
        \]

        de plus sa construction est effective.
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Streaming string transducers: Théorèmes}

    \begin{alertblock}{Équivalence \cite{alur2011}}
    Le problème suivant est dans \PSPACE~:
    
        Étant donnés $T_1$ et $T_2$ deux \emph{streaming stream transducers},
        définissent-ils les mêmes fonctions partielles ?

    \end{alertblock}

    \begin{alertblock}{Typechecking \cite{alur2011}}
    	 Le problème suivant est dans \PSPACE~:
	 
        Étant donné un \emph{streaming string transducer} T,
        deux formules MSO sur les mots $\phi$ et $\psi$, est ce que~:
        
        \[
            \forall w, w \vDash \phi \Rightarrow T(w) \vDash \psi 
        \]

    \end{alertblock}

\end{frame}

\begin{frame}
    \frametitle{Conclusion}

    \begin{itemize}
    
    \item une théorie complétée depuis peu
    
    \ds
    
    \item des applications à l'analyse de programmes
    
    \ds

    \item encore des problèmes ouverts
    
    \ds

    \end{itemize}

\end{frame}


\section*{Bibliographie}

\begin{frame}[allowframebreaks]

\bibliographystyle{apalike}
\bibliography{presentation}

\end{frame}

\end{document}
