# Présentation Initiation à la Recherche

Les articles se trouvent dans le dossier « Articles ».

Le plan est le suivant

Introduction :
-> De quoi on va parler : transducteurs qui caractérisent donc des relations entre mots, pas comme les automates qui font des langages
-> A quoi ça sert  de faire ça ?
-> On fait ça depuis longtemps, il y a des gens connus, mais nouveaux résultats.
-> Conférences

1. Paysage général des transducteurs & liens avec la logique
    1. Automates : Machines de turing sans écriture
        1. MSO <-> Monoide syntaxique fini <-> Automate 
        2. FO  <-> Monoide syntaxique apériodique <-> Automate apériodique <-> Automate sans compteur 
    2. Considérations sur les machines de turing : problèmes de décisions, problèmes de relations 
    3. Transducteurs synchrones (facile)
    4. Fonctions séquentielles (un peu plus dur)
    5. Transducteurs 2-WAY déterministes
2. Quelques résultats récents
   1. Caractérisation logique des 2 WAY transducers
       1. Transductions MSO sur des graphes + exemples
       2. Le cas des mots vus comme des graphes
       3. Equivalence : idee de la preuve
       3. Ajouter du non-déterminisme
    2. FO transductions
       1. Parallèle avec le théorème sur les automates
       2. Idée de la preuve
       3. Des résultats incomplets : pas cool 
3. Application à la vérification
(ahah)