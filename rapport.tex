\documentclass[a4paper,10pt]{article}
\usepackage[a4paper]{geometry}
\geometry{ left=2cm, right=2cm,top=2.5cm ,bottom=3cm}


\usepackage{palatino}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath,amssymb}%for \mod, \bmod, ..
\usepackage{amsthm,amsfonts,mathrsfs}
\usepackage[full,small]{complexity}
\usepackage{ stmaryrd }

\usepackage{multirow}

\usepackage{ marvosym }

%pour le tableau récapitualif 
\usepackage{hyperref}

\usepackage{multicol}
\usepackage{enumerate}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\newtheorem{q}{Question}
\newtheorem{lemma}{Lemme}
\newtheorem{definition}{Définition}


\newcommand{\eqv}{\Leftrightarrow}
\newcommand{\ds}{\vspace{0.5\baselineskip}}

\title{\textbf{Transducteurs \& Logique} \\ 
 Initiation à la Recherche}
\author{\bsc{Douéneau} Gaëtan \and \bsc{Lopez} Aliaume}

\begin{document}
\maketitle


\begin{abstract}


La théorie des automates finis, initiée au milieu du vingtième siècle, a mis au jour
de nombreuses caractérisations des langages rationnels. La logique \textsf{MSO}
interprétée sur les mots offre en particulier une représentation équivalente, 
et la connexion entre logique et automates est l'un des piliers permettant 
de montrer que
la plupart des modèles de décision proches des automates finis
déterministes ont un pouvoir expressif identique. C'est par exemple le cas des
automates boustrophédons \cite{jc1959}.

Les transducteurs finis sont une extension des automates, et permettent de
représenter des relations entre mots plutôt que des langages. L'objectif de ce
rapport est d'étudier les développements récents quant au pouvoir expressif des
différents types de transducteurs, en parallèle avec les résultats établis sur
les automates finis. Nous présenterons particulièrement des caractérisations
logiques des relations reconnues par certains transducteurs.

\end{abstract}

\section{Introduction du sujet}

\subsection{Contexte historique}

Les premiers résultats importants concernant les relations entre logique et automates finis 
datent des années 1950-60. Assez rapidement, la théorie est devenue «~complète~»~:
son paysage est clairement 
défini et les questions fondamentales on trouvé une réponse pertinente.
En est témoin l'enseignement des bases des automates finis dès la licence d'informatique.

\ds

La notion de \emph{transducteur fini} a été étudiée en parallèle, 
mais il faudra attendre 2001 et le papier 
fondateur \cite{engelfriet2001} pour établir la première correspondance
entre la Logique Monadique du Second Ordre (\textsf{MSO}) et les
relations décrites par certains transducteurs.

Bien que ces résultats profonds datent du début des années 2000,
on peut remarquer que la majorité des théorèmes du domaine
ont été prouvés après 2010. Sans surprise, ce sujet de recherche est en vogue 
dans les conférences théoriques comme CSL ou LICS.

\subsection{Les différents acteurs}


Les principaux laboratoires français travaillant actuellement sur ces sujets
sont l'\emph{Institut de Recherche en Informatique Fondamentale} (IRIF) de l'Université Paris 7 et son équipe de recherche en théorie des automates, ou le \emph{Laboratoire Bordelais de Recherche en Informatique} (LaBRI) de l'Université de Bordeaux, en méthodes formelles.

\ds

On retrouve à l'étranger l'équipe de \emph{Méthodes Formelles et Vérification} de l'Université 
de Bruxelles ou l'équipe \emph{Regular Functions and Quantitative Properties 
of Data Streams} de l'Université de Pennsylvanie. Cette dernière héberge notamment 
Rajeev Alur, très connu dans le domaine (et dans d'autres).


\subsection{Publication des développements récents}



\cite{carton2015} explore les transductions définissables par la logique du premier ordre \textsf{FO} (vue comme une restriction de l'expressivité de \textsf{MSO}). Il a été publié dans \textbf{Computer Science Logic} (CSL).

\ds


D'autres papiers purement théoriques ont été acceptés à \textbf{Logic In Computer Science} (LICS), comme 
\cite{Dartois2016TwoWayVP} dont l'objectif est d'étendre une fois de plus 
les liens connus entre les transducteurs et la logique. Ils prouvent en outre un certain nombre de résultats de décidabilité.

\ds

La conférence qui publie actuellement le plus d'articles sur le sujet est \textbf{Principles Of Programming Languages} (POPL), avec par exemple \cite{Lin2016StringSW}, \cite{Botbol2016StaticAO} ou \cite{Grathwohl2016KleenexCN}. Notons que l'approche générale de ces recherches est orientée vers des applications à la programmation ou à la vérification, en utilisant les théories logiques mais sans qu'elles soient le centre de l'attention.

\section{Etat de l'art}

\subsection{Zoologie des transducteurs}

Un automate fini représente un langage $L \subseteq A^*$. La définition d'un transducteur est une légère modification de la définition d'un automate, en l'autorisant à écrire une sortie (sur un alphabet $B^*$) au fur et à mesure de ses transitions. Il reconnaît donc une relation $R \subseteq A^*\times B^*$.

\ds

Contrairement au cas des automates finis, de légères modifications dans la définition d'un transducteur (présence de non-déterminisme, possibilité de lire l'entrée plusieurs fois\dots) induisent une hiérarchie stricte d'expressivité. Nous en présentons une partie dans la Table \ref{auto-trans} ci-dessous, en comparant avec les modèles d'automates associés (qui eux sont tous équivalents).

Pour des définitions précises des modèles, on pourra se rapporter à \cite{filiot2015} ou \cite{carton2015}.

\begin{table}[h!]
\begin{center}
    \begin{tabular}{r||l}
        \textbf{Automates} & \textbf{Transducteurs} \\ \hline
        \multicolumn{2}{c}{Transducteur synchrone} \\ \cline{2-2}
        Déterministes      & Fonction séquentielle \\ \cline{2-2}
        Non déterministes  & Transducteur rationnel \\ \cline{2-2}
        Boustrophédons  déterministes   & Transducteur à deux sens déterministes
    \end{tabular}
\end{center}
\caption{\label{auto-trans} Correspondance entre automates et transducteurs}
\end{table}

Nous avons choisi de nous restreindre uniquement aux \textbf{transducteurs qui définissent des fonctions}. C'est automatiquement le cas lorsqu'ils sont déterministes, mais cela correspond à une forme de non-ambiguité pour les transducteurs non-déterministes (la relation qu'ils définissent doit être une fonction (partielle) $A^* \rightarrow B^*$).

\subsection{Transducteurs et logique}

Un langage est reconnu par un automate fini si et seulement s'il existe une
formule \textsf{MSO} interprétée sur les mots qui code exactement ce langage. Ce résultat et ses variantes sur les mots infinis, les arbres (finis ou infinis)\dots~sont à la base de la preuve de nombreux théorèmes de décidabilité.

\ds

Trouver une caractérisation logique des fonctions décrites par certaines
classes de transducteurs finis est donc particulièrement pertinent.
Cependant, il est clair qu'une interprétation «~simple~» de \textsf{MSO} sur les mots sera insuffisante, dans la mesure où elle permet de représenter uniquement des langages $L \subseteq A^*$, mais pas des fonctions (partielles) $A^* \rightarrow B^*$. Il faut donc «~enrichir~» légèrement l'interprétation.

Les \emph{\textsf{MSO}-transductions}, introduites dans \cite{courcelle92} puis
étendues dans \cite{courcelle94}, répondent à ce problème. Elles étaient à l'origine définies comme des transformations de graphes.
Informellement, une \textsf{MSO}-transduction est une collection de formules
\textsf{MSO} qui décrivent comment modifier un mot pour calculer son (éventuelle) image. Elle permet ainsi de coder une fonction (partielle).

\ds

Le résultat fondateur que nous évoquions dans la section précédente, issu de \cite{engelfriet2001}, tisse une équivalence entre des \emph{transducteurs déterministes à deux sens} (2DFT) et les \textsf{MSO}-transductions dans toute leur généralité.

\ds

Ce théorème a été raffiné de nombreuses fois par la suite, pour les sous-classes de transducteurs ou des transductions logiques n'utilisant que des fragments de \textsf{MSO} (commme \textsf{FO} ou \textsf{MSO}$_{op}$ - les formules préservant un certain ordre). Pour mémoire, nous les listons dans la Table \ref{logic-trans}, avec la référence où trouver le détail de leur énoncé et la preuve.

Nous n'entrons pas dans le détail des définitions, mais cela permet néanmoins de se représenter la structure générale des équivalences transducteurs-logique.

\ds

\begin{table}[h!]
   
    \begin{center}
        \begin{tabular}{lcl|l}
            \textbf{Transducteur} & & \textbf{Logique} & \textbf{Référence} \\ \hline \hline  
            déterministe à deux sens (2DFT) & $\leftrightarrow$ & \textsf{MSO}-transductions  & \cite{engelfriet2001} \\
             non-déterministe à un sens (NFT) & $\leftrightarrow$ & \textsf{MSO}$_{op}$-transductions & \cite{filiot2015} \\
            2DFT apériodique  & $\leftrightarrow$ & \textsf{FO}-transductions  & \cite{carton2015} \\
            NFT apériodiques   & $\leftrightarrow$ & \textsf{FO}$_{op}$-transductions  & \cite{filiot2016} \\
        \end{tabular}
    \end{center}
    \caption{\label{logic-trans} Liens entre la logique et les transducteurs}
\end{table}

\subsection{Applications à la vérification}


Les transducteurs et en particulier leurs liens avec la logique \textsf{MSO}
donnent des résultats de décidabilité intéressants qui trouvent 
une application directe en vérification. 

\ds

L'exemple le plus probant est
celui des \emph{Streaming String Transucers} (SST) \cite{alur2011} 
dont la théorie permet de vérifier que deux programmes traitant des listes 
(avec des hypothèses raisonnables) calculent effectivement la même 
fonction (\emph{i.e.} ont la même sémantique dénotationnelle).

Dans la même veine, on obtient un résultat plus surprenant encore :
sur la même classe d'objets, on peut décider de la validité
de certaines propriétés du programme. 
Cela permet par exemple de tester la correction du programme, 
dès qu'elle peut s'exprimer comme une formule \textsf{MSO} sur les mots.

\ds


Bien que la classe des programmes concernés semble restreinte, elle possède une 
expressivité tout à fait pertinente. De plus la complexité de la décision n'est pas au-delà de \PSPACE. Il n'est donc pas difficile d'imaginer
utiliser de telles caractérisations sur des programmes traitant 
des bases de données. 


\section{Un problème ouvert}

Il est possible de déterminer si un transducteur déterministe à deux sens (2DFT) est apériodique \cite{carton2015} en analysant sa structure. Ce résultat ne permet cependant pas de savoir si la fonction réalisée par un 2DFT quelconque pourrait être 
réalisée par un 2DFT apériodique ! \cite{carton2015}

\ds

Cette question n'a pas encore été résolue. Elle est fondamentale car directement en rapport 
avec une description \emph{sémantique} des fonctions sur les mots (les \textsf{FO}$_{op}$-transductions)
qui serait indépendante des considérations \emph{syntaxiques} ou des 
propriétés des modèles de calculs associés.
En effet, il semble difficile de vérifier cette propriété en regardant uniquement les formules logiques. On peut s'en convaincre en 
constatant que la propriété équivalente pour les automates 
est prouvée directement en utilisant la structure des automates (la présence ou non de compteurs).

\ds

Néanmoins, ce sujet pourrait être résolu dans un stage, dans la mesure où certaines «~pistes~» sont déjà ouvertes. En outre un problème similaire a déjà été résolu : savoir si une fonction définie par un transducteur non-déterministe à un sens pourrait être définie par un transducteur non-déterministe apériodique \cite{filiot2016}.

\ds

Plus généralement (développement possible du sujet de stage vers une thèse brillante), la définition d'un transducteur «~canonique~» - qui mimerait 
certaines propriétés de l'\emph{automate minimal} -  permettrait de répondre systématiquement à ce genre de questions. La construction d'un tel objet serait une avancée majeure dans une compréhension fine de la hiérarchie des transducteurs.

\pagebreak

\bibliographystyle{alpha}
\bibliography{presentation}

\end{document}

